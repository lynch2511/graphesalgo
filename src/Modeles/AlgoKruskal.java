package Modeles;

import Graphe.*;

import java.util.ArrayList;

public class AlgoKruskal extends Algorithme {

    private int[] _cfc;
    private Graphe _graphe;
    private int[] _NbElem;
    private int[] _pilch;
    private int[] _prem;

    public AlgoKruskal(String nom, String description, Graphe g) {
        super(nom, description);
        _graphe = g;
        _cfc = new int[g.nbSommets()+1];
        _NbElem = new int[g.nbSommets()+1];
        _prem = new int[g.nbSommets()+1];
        _pilch = new int[g.nbSommets()+1];
        for(int i=1; i<=g.nbSommets();i++)
        {
            _prem[i] = i;
            _pilch[i] = 0;
            _cfc[i] = i;
            _NbElem[i] = 1;
        }
        g.matPoidsAdj2Aretes();
        g.trierAretes();
    }

    public AlgoKruskal(String nom, String description) {
        super(nom, description);
    }

    /**
     * Permet d'obtenir le graphe réduit déduit de l'algorithme de Kruskal
     * @return un nouveau Graphe.Graphe
     */
    public Graphe Kruskal()
    {
        Graphe out = new Graphe(_graphe);
        out.set_aretes(new ArrayList<Arete>()); // _graphe.nbSommets()-1

        int x; // respectivement le num�ro de composante de la 1�re  extr�mit� de l�ar�te courante
        int y; // respectivement le num�ro de composante de la 2�me  extr�mit� de l�ar�te courante
        int i = 0, j = 0;//respectivement indice dans g et t
        while (j < _graphe.nbSommets()-1)
        {
            Arete ar = _graphe.get_aretes().get(i);
            x = _cfc[ar.getSommetDepart()];
            y = _cfc[ar.getSommetArrivee()];
            if (x != y)
            {
                out.get_aretes().add(j++, _graphe.get_aretes().get(i));

                fusionner(x, y);
            }
            i++;
        }
        out.tabAretes2MatAdj(_graphe.nbSommets());
        return out;
    }

    public void fusionner(int i, int j)
    {
        if (_NbElem[i] < _NbElem[j])
        {
            int aux = i;
            i = j;
            j = aux;
        }
        int s = _prem[j];
        _cfc[s] = i;
        while (_pilch[s] != 0)
        {
            s = _pilch[s];
            _cfc[s] = i;
        }
        _pilch[s] = _prem[i];
        _prem[i] = _prem[j];
        _NbElem[i] += _NbElem[j];
    }

}
