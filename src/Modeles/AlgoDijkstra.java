package Modeles;

public class AlgoDijkstra extends Algorithme {

    public AlgoDijkstra(String nom, String description, int[] fs, int[] aps, int[][] matPoids, int sDepart) {
        super(nom, description);
        this._fs = fs;
        this._aps = aps;
        this._matPoids = matPoids;
        this._sDepart = sDepart;
    }

    public AlgoDijkstra(String nom, String description) {
        super(nom, description);
    }

    private int[] _fs;
    private int[] _aps;
    private int[][] _matPoids;
    private Integer _sDepart;
    private int[] _distances;
    private int[] _pred;

    /**
     * Calcul du plus court chemin depuis le sommet de départ
     * @return Tableaux distances (indice 0) et tableau des prédécesseurs (indice 1)
     */
    public int[][] Dijkstra()
    {
        if(_aps != null && _fs != null)
        {
            int ind; // nombre d'élements qui restent à traiter
            int k,v;
            int j = 0;
            int n = _aps[0];
            int m = _fs[0];
            int[][] resultat= new int[2][2]; // tableau de sortie
            _pred = new int[n+1];
            _distances = new int[n+1];
            int[] inS = new int[n+1]; // tab boolens pour indiquer quels sommets restent à traiter
            // intialisation des tableaux d, prem et inS
            for(int i = 1; i<= n; i++)
            {
                _distances[i] = _matPoids[_sDepart][i];
                inS[i] = 1;
                _pred[i]=-1;
                if(i == _sDepart)
                {
                    _pred[i] = 0;
                }
                else
                {
                    _pred[i] = -1;
                }
            }
            _distances[_sDepart] = 0;
            _pred[_sDepart] = 0;
            inS[_sDepart]=0;
            ind = n-1;
            while (ind>0) // tant qu'il y a des sommets à traiter
            {
                // calcul du minimum selon d des sommets de S
                m = Integer.MAX_VALUE;
                for(int i = 1;i<=n;i++)
                {
                    if(inS[i] == 1)
                    {
                        if(_distances[i] < m )
                        {
                            m = _distances[i];
                            j = i;
                        }
                    }
                }
                if(m == Integer.MAX_VALUE)
                {

                    resultat[0] = _distances;
                    resultat[1] = _pred;
                    return resultat; // les sommets restants sont inaccessibles
                }
                inS[j] = 0;
                ind--;
                k=_aps[j];
                while(_fs[k] != 0)
                {
                    if(inS[_fs[k]] ==1) // le successeur n'a pas encore été traité
                    {
                        v = _distances[j]+_matPoids[j][_fs[k]];
                        if(v<_distances[_fs[k]])
                        {
                            _distances[_fs[k]] = v;
                            _pred[_fs[k]] = j;
                        }
                    }
                    k++;
                }
            }
            resultat[0] = _distances;
            resultat[1] = _pred;
            return resultat;
        }
            return null;
    }

}
