package Modeles;

import org.apache.commons.lang3.ArrayUtils;

public class AlgoTarjan extends Algorithme{

    private int[] prem;
    private int[] pilch;
    private int[] cfc;
    private int[] pred;
    private int[] tarj;
    private boolean[] entarj;
    private int[] num;
    private int[] ro;
    private int[] _aps;
    private int[] _fs;
    private int[] fsr;
    private int[] apsr;
    private int p;
    private int inter;
    private int compteur =0;

    public AlgoTarjan(String nom, String description) {
        super(nom, description);
    }
    public void fortconnexe(int[] aps, int[] fs)
    {
        _aps = aps;
        _fs = fs;
        int n = aps[0];
        p = 0;
        prem = new int[n+1];
        pilch = new int[n+1];
        cfc = new int[n+1];
        pred = new int[n+1];
        tarj = new int[n+1];
        entarj = new boolean[n+1];
        num = new int[n+1];
        ro = new int[n+1];
        inter = 0;
        for (int i = 1; i<=n; i++)
        {
            num[i] = 0;
            pred[i] = 0;
            ro[i] = 0;
            entarj[i] = false;
        }
        pilch[0] = 0;
        while(p<=aps[inter] && p<aps.length)
        {
            if(num[inter+1]==0)
            {
                if(inter==0)
                {
                    traversee(inter);
                }
                else
                {
                    traversee(inter+1);
                }
            }
        }
        // graphereduit
        //editer base
        /*
        for(int s = 1; s<=aps[0]; s++)
            if (num[s] == 0) traversee(s);
        prem[0] = inter;

        tarj = null;
        entarj = null;
        num=null;
        ro = null;

         */
    }

    public void traversee(int s)
    {
        if(tarj != null && ro != null && entarj != null && _fs != null && _aps != null)
        {
            if(inter<s+1)
            {
                inter =s;
            }
            p++; num[s] = p; ro[s] = p;
            empiler(s+1,tarj);
            entarj[s] =true;
            int t;
            for(int k = _aps[s] ; (_fs[k]!=0);k++)
            {
                t=_fs[k];
                if(num[t-1]==0)
                {
                    pred[t-1] = s+1;
                    traversee(t-1);
                    if(ro[t-1]<ro[s])
                    {
                        ro[s]=ro[t-1];
                    }
                }
                else
                {
                    if(num[t-1] < ro[s] && entarj[t-1])
                    {
                        ro[s]=num[t-1];
                    }
                }
            }
            if(ro[s] == num[s])
            {
                compteur++;
                int u = depiler(tarj);
                while(u!=s+1)
                {
                    entarj[u-1] = false;
                    empiler(u,pilch);
                    cfc[u-1] = compteur;
                    u=depiler(tarj);
                }
                cfc[u-1] = compteur;
                entarj[s] = false;
                empiler(s+1,pilch);
                pilch[0] = 0;
                prem[compteur-1] = s+1;
            }
        }

    }

    /*
    Permet d'obtenir les tableaux fs et aps du graphe réduit des composantes fortement connexes
     */
    public void graph_reduit (int[] prem,int[] pilch,int[] cfc,int[] fs, int[] aps)
    {
        int s, kr=1, nbc=prem[0];
        boolean[] deja_mis=new boolean[nbc+1];
        fsr=new int [fs[0]+1];
        apsr=new int [nbc+1];
        apsr[0]=kr;
        for(int i=1;i<nbc;i++)
        {
            apsr[i]=kr;
            for(int j=1;j<=nbc;j++)
            {
                deja_mis[j]=false;
            }
            deja_mis[i]=true;
            s=prem[0];
            while(s!=0)
            {
                for(int t=aps[s];fs[t]!=0;t++)
                {
                    if(deja_mis[cfc[fs[t]]]==false)
                    {
                        fs[kr]=cfc[fs[t]];
                        kr++;
                        deja_mis[cfc[fs[t]]]=true;
                    }
                }
                s=pilch[s];
            }
            fsr[kr]=0;
            kr++;
        }
        fsr[0]=kr-1;
        deja_mis=null;
    }

    public void empiler (int x, int[] pilch) // avec x dans {1, ... , n}
    {
        pilch[x] = pilch[0];
        pilch[0] = x;
    }

    public int depiler(int[] tarj)
    {
        int u = tarj[0];
        tarj = ArrayUtils.remove(tarj,0);
        return u;
    }

    public int[] getPrem() {
        return prem;
    }

    public int[] getPilch() {
        return pilch;
    }

    public int[] getCfc() {
        return cfc;
    }

    public int[] getPred() {
        return pred;
    }

    public int[] getFsr() {
        return fsr;
    }

    public int[] getApsr() {
        return apsr;
    }
}
