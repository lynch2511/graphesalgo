package Modeles;

import Vues.afficheurCLI;

public class AlgoDantzig extends Algorithme{

    private int[][] matCoutsDistances;

    public int[][] getMatCoutsDistances() {
        return matCoutsDistances;
    }

    public void setMatCoutsDistances(int[][] matCoutsDistances) {
        this.matCoutsDistances = matCoutsDistances;
    }

    public AlgoDantzig(String nom, String description) {
        super(nom, description);
    }

    public boolean Dantzig(int[][] c)
    {
        // matrice des coûts qui sera remplacée par la matrice des distances
        //pré traitement)
        int n = (int) c[0][0];

        matCoutsDistances = new int[0][0];
        int x;
        int k,i,j;
        for(k=1; k< n;k++)
        {
            for(i = 1; i<=k; i++)
            {
                for(j=1;j<=k; ++j)
                {
                    if((x = c[i][j] + c[j][k+1])< c[i][k+1] && c[i][j] != Integer.MAX_VALUE && c[j][k+1] != Integer.MAX_VALUE && c[i][k+1] != Integer.MAX_VALUE)
                    {
                        c[i][k+1] = x;
                    }
                    if((x=c[k+1][j]+c[j][i])<c[k+1][i] && c[k+1][j] != Integer.MAX_VALUE && c[j][i] != Integer.MAX_VALUE && c[k+1][i] != Integer.MAX_VALUE)
                    {
                        c[k+1][i] =x;
                    }
                }
                if(c[i][k+1]+c[k+1][i]<0 && c[i][k+1] != Integer.MAX_VALUE && c[k+1][i] != Integer.MAX_VALUE)
                {
                    afficheurCLI.print("Présence d'un circuit absorbant passant par "+i+" et "+k+1+"\n");
                    //return false;
                }
            }
            for(i = 1; i<=k;i++)
            {
                for(j=1 ; j<=k ; j++)
                {
                    if((x=c[i][k+1]+c[k+1][j])<c[i][j] && c[i][k+1] != Integer.MAX_VALUE && c[k+1][j] != Integer.MAX_VALUE && c[i][k+1] != Integer.MAX_VALUE)
                    {
                        c[i][j]=x;
                    }
                }
            }
        }
        matCoutsDistances = c;
        return true;
    }
}
