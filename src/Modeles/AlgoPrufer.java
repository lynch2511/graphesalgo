package Modeles;

public class AlgoPrufer extends Algorithme {

    private int[] _prf;
    private int[][] _poidsAdj;

    public AlgoPrufer(String nom, String description, int[][] poidsAdj) {
        super(nom, description);
        this._poidsAdj = poidsAdj;
    }

    public AlgoPrufer(String nom, String description) {
        super(nom, description);
    }

    /**
     * Permet de coder un graphe non-orienté et non valué selon l'algorithme de prufer
     * @return tableau de codage du graphe. Indice 0 : nombre de sommets -2
     */
    public int[] CodageAlgoPrufer()
    {
        // réappliquer le pré traitement :

        int nb_som = _poidsAdj[0][0];

        for(int i =1;i<= nb_som;i++)
        {
            _poidsAdj[i][0] = 0;
            for(int j=1; j<= nb_som;j++)
            {
                if(_poidsAdj[i][j] == 1)
                {
                    _poidsAdj[i][0]++;
                }
            }
        }



        _prf = new int[nb_som-1];
        _prf[0] = nb_som-2;
        int k = 1;
        while (k <= nb_som-2)
        {
            int i= 1;
            int j=1;
            while(_poidsAdj[i][0] != 1 && i < nb_som)
            {
                i++;
            }
            while(_poidsAdj[i][j] != 1 && j < nb_som)
            {
                j++;
            }
            _prf[k++]=j;
            _poidsAdj[i][j]=Integer.MAX_VALUE;
            _poidsAdj[j][i]=Integer.MAX_VALUE;
            _poidsAdj[i][0]=0;
            _poidsAdj[j][0]--;
        }
        return _prf;
    }

    public int[][] decodage(int[] p)
    {
        int m = p[0];
        int n=m+2;
        int[][] matAdj = new int[n+1][n+1];
        // initialiser la matricepoidsAdjacence
        for(int i =1; i<= n; i++)
        {
            for(int j = 1; j<= n; j++)
            {
                matAdj[i][j] = Integer.MAX_VALUE;
            }
        }
        matAdj[0][0] = n;
        int[] I= new int[n+1]; // 3e tableau permettant de direr combien de fois chaque valeur est dans p.

        for(int i=1; i<= n ; i++) { I[i] = i;} // initialisation de I
        int j = 0;
        int s = 1; // premier élément de p
        for(int i=1; i<=n; i++)
        {
            j = minimumNotInP(I,p);
            while(I[j] != 0 && s < p.length)
            {
                    matAdj[p[s]][j] = 1;
                    matAdj[j][p[s]] = 1;
                    matAdj[p[s]][0]++;
                    matAdj[0][1]++;
                    I[j] = 0;
                    p[s] = 0;
                    s++;
            }
        }
        // deux derniers sommets dans I
        for (int i = 1; i<= n;i++)
        {
            if(I[i] != 0 && I[i] != s)
            {
                matAdj[s][I[i]] = 1;
                matAdj[I[i]][s] = 1;
                matAdj[I[s]][0]++;
                matAdj[0][1]++;
            }
        }
        return matAdj;
    }

    private int minimumNotInP(int[] tab,int[] p)
    {
        int min = Integer.MAX_VALUE;
        for(int i = 1; i<tab.length;i++)
        {
            if(tab[i] > 0 && tab[i] < min && notIn(p,tab[i]))
            {
                min = tab[i];
            }
        }
        return min;
    }

    private boolean notIn(int[] p, int j)
    {
        for (int i = 1; i<p.length; i++)
        {
            if(p[i] == j)
            {
                return false;
            }
        }
        return true;
    }


}
