package Modeles;

import Graphe.*;

public class AlgoOrdonnancement extends Algorithme {

    private int[] _fpc; // tableau des prédécesseurs critiques
    private int[] _appc; // adresse des premiers prédécesseurs critiques
    private int[] _lc; // Tableau des longueurs critiques
    private Graphe _g;

    public AlgoOrdonnancement(String nom, String description) {
        super(nom, description);
    }

    public AlgoOrdonnancement(String nom, String description, Graphe g) {
        super(nom, description);
        this._g = g;
    }

    public int[] get_fpc() {
        return _fpc;
    }

    public int[] get_appc() {
        return _appc;
    }

    public int[] get_lc() {
        return _lc;
    }

    public void ordonnancement(int[] fp, int[] app, int[] d)
    {
        int n = app[0], m = fp[0];
        _fpc = new int[m+1];
        _appc = new int[n+1]; _appc[0] = n;
        _lc = new int[n+1]; _lc[0] = n;
        int kc, t, lg;
        _lc[1] = 0;
        _fpc[1] = 0; // fin de la liste
        _appc[1] = 1;
        kc = 1; // indice de la dernière place remplie dans fpc
        for(int s = 2; s <= n; s++)
        {
        //calcul de lc[s] en fonction des prédécesseurs de s
            int k=0;
            _lc[s] = 0;
            _appc[s] = k+1; // début de la liste des prédécesseurs critiques de s
            for (k = app[s]; (t = fp[k]) != 0; k++)
            {
                lg = _lc[t] + d[t];
                if (lg >= _lc[s])
                if (lg > _lc[s])
                {
                    _lc[s] = lg; // Nouvelle lg candidate a être critique
                    kc = _appc[s];
                    _fpc[kc] = t;
                }
                else // lg == lc[s] : ajouter un nouveau prédécesseur critique
                {
                    kc++;
                    _fpc[kc] = t;
                }
            } //for k
            kc++ ;
            _fpc[kc] = 0; //fin de la liste des prédécesseurs critiques de s
        } //for s
        _fpc[0] = kc;
    }

    // déterminer le tableau des sommets de degré demi intérieur
    public int[] det_ddi()
    {
        int[] _fs = _g.get_fs();
        int[] _aps = _g.get_aps();
        if(_fs != null && _aps != null)
        {
            int n = _aps[0];
            int[] ddi = new int[n+1];
            ddi[0] = n;
            for(int s = 1; s<= n; s++)
            {
                ddi[s] = 0;
            }
            for(int k =1; k < _fs[0]; k++)
            {
                if(_fs[k] != 0)
                {
                    ddi[_fs[k]]++;
                }
            }
            return ddi;
        }
        return null;
    }

    // Déterminer l'adresse des premiers prédécesseurs
    public int[] det_app(int[] ddi)
    {
        int[] _aps = _g.get_aps();
        if(_aps != null && ddi != null)
        {
            int n=_aps[0];
            int[] app = new int[n+1];
            app[0] = n;
            app[1] = 1;
            for(int i=1; i<n; i++)
                app[i+1] = app[i] + ddi[i] + 1;
            return app;
        }
        return null;
    }

    // Déterminer la file des prédécesseurs
    public int[] det_fp(int[] app)
    {
        int[] _fs = _g.get_fs();
        int[] _aps = _g.get_aps();
        if(_fs != null && _aps != null && app != null)
        {
            int n = _aps[0];
            int m = _fs[0];
            int[] fp = new int[m+1];
            fp[0] = m;
            int k=1,j;
            for(int i=1; i<=n; i++)
            {
                while ((j=_fs[k]) > 0)
                {
                    fp[app[j]] = i;
                    app[j]++;
                    k++;
                }
                k++;
            }
            fp[app[n]] = 0;
            for(int i=n-1; i>=1; i--)
            {
                fp[app[i]] = 0;
                app[i+1] = app[i] + 1;
            }
            app[1] = 1;
            return fp;
        }
        return null;
    }

    // Déterminer le tableau des durées des tâches
    public int[] det_d()
    {
        int[] _fs = _g.get_fs();
        int[] _aps = _g.get_aps();
        Sommet[] _sommets = _g.get_sommets();
        if(_fs != null && _aps != null && _sommets != null)
        {
            int n = _aps[0];
            int t;
            int[] d = new int[_aps[0]+1];
            for(int s = 1; s<=n;s++)
            {
                while((t =_fs[_aps[s]])!=0)
                {
                    d[s] = _sommets[s].getDuree();
                    s=t;
                }
            }
            return d;
        }
        return null;
    }
}
