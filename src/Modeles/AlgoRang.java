package Modeles;

import Graphe.Graphe;

public class AlgoRang extends Algorithme {

    public AlgoRang(String nom, String description, int[] fs, int[] aps) {
        super(nom, description);
        this._fs = fs;
        this._aps = aps;
     }

    public AlgoRang(String nom, String description) {
        super(nom, description);
    }

    private int[] _fs;
    private int[] _aps;
    private int[] _ddi;
    private int[] _pilch;
    private int[] _prem;

    public static boolean applicable(Graphe g)
    {
        if(g.grapheOriente() && g.circuits()==false) {return true;}
        return false;
    }

    public int[] rang()
    {
        Integer n = _aps[0];
        Integer tailleFs = _fs[0];
        Integer s = 0;
        Integer k = 0;
        Integer h = 0;
        Integer t = 0;
        int[] rang = new int[n+1];
        _ddi = new int[n+1]; // nombre de prédécesseurs pour chaque sommet d'indice i
        _pilch = new int[n+1];
        _prem = new int[n+1];
        //calcul de ddi prédécesseurs de i
        for (s =1; s<= n;s++)
        {
            _ddi[s]=0;
        }
        for(int i=1; i <=tailleFs ; i++)
        {
            s=_fs[i];
            if (s >0) _ddi[s]++; // combien de prédécesseurs possède le sommet s
        }

        //calcul du rang
        _pilch[0]=0;
        for(s = 1; s <= n; s++)
        {
            rang[s] = -1; // n : nombre de sommets de G represente l'infini
            if (_ddi[s] == 0) empiler(s,_pilch); // on empile les sommets sans prédécesseurs
        }
        k=-1;
        s=_pilch[0];
        _prem[0] = s;
        while (_pilch[0] > 0)
        {
            k++;
            _pilch[0] = 0;
            while (s > 0)
            {
                rang[s] = k;
                h = _aps[s]; t = _fs[h];
                while (t > 0) // successeurs du sommet de niveau k
                {
                    _ddi[t]--; // on retire un prédécesseurs de t, soit s
                    if (_ddi[t] == 0) empiler(t,_pilch);
                    h++;
                    t=_fs[h];
                }
                s = _pilch[s];
            }
            s = _pilch[0];
            _prem[k+1] = s;
        }
        return rang;
    }

    public void empiler(int x, int[] pilch)
    {
        pilch[x] = pilch[0];
        pilch[0] = x;
    }
}
