import Graphe.Graphe;
import Graphe.Sommet;
import eu.jacquet80.minigeo.MapWindow;
import eu.jacquet80.minigeo.POI;
import eu.jacquet80.minigeo.Point;
import eu.jacquet80.minigeo.Segment;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.*;

public class ApplicationExemple {

    private String FICHIERSOURCECARTE = "france.poly";
    private static Pattern POINT = Pattern.compile("^.*?(-?\\d+\\.\\d+)\\s+(-?\\d+\\.\\d+)$");
    private Graphe g;
    private int nbSommets;

    public ApplicationExemple(int nbSommets) {
        this.nbSommets = nbSommets;
        g = new Graphe(true,false,false);
    }

    public void initialiserVilles()
    {
        if(g != null)
        {
            Sommet[] villes = new Sommet[nbSommets];
            villes[0] = new Sommet(0);
            villes[1] = new Sommet(1,6.0240539,47.237829, "Besançon");
            villes[2] = new Sommet(2,7.335888 ,  47.750839,"Mulhouse");
            villes[3] = new Sommet(3,	4.835659, 	45.764043,"Lyon");
            villes[4] = new Sommet(4,2.3522219, 48.856614,"Paris");
            villes[5] = new Sommet(5, 	-1.553621,47.218371, "Nantes");
            villes[6] = new Sommet(6, 5.724524,	45.188529, "Grenoble");
            villes[7] = new Sommet(7, 7.2619532,43.7101728,"Nice");
            villes[8] = new Sommet(8, 5.36978, 43.296482, "Marseille");
            villes[9] = new Sommet(9, -0.57918,44.837789,"Bordeaux");
            villes[10] = new Sommet(10,	1.444209,	43.604652, "Toulouse");
            villes[11] = new Sommet(11,	1.909251,47.902964, "Orléans");
            villes[12]= new Sommet(12,3.087025,45.777222, "Clermont-Ferrand");
            villes[13] = new Sommet(13, 	3.057256,	50.62925,"Lille");
            villes[14] = new Sommet(14,	7.7521113,	48.5734053,"Strasbourg");
            villes[15] = new Sommet(15, -1.6777926,	48.117266,"Rennes");
            villes[16] = new Sommet(16, 5.04148,	47.322047,"Dijon");
            villes[17] = new Sommet(17, 	2.398782,	47.081012,"Bourges");
            villes[18] = new Sommet(18, 3.573781,	47.798202,"Auxerre");
            villes[19] = new Sommet(19,	3.876716,43.610769,"Montpellier");
            villes[20] = new Sommet(20,	1.099971,49.443232,"Rouen");
            villes[21] = new Sommet(21, 	2.295753,49.894067, "Amiens");
            villes[22] = new Sommet(22,	2.777535,	50.291002,"Arras");
            villes[23] = new Sommet(23, 6.1757156,49.1193089,"Metz");
            g.set_sommets(villes);
            g.initialiserMatPoidsAdj(nbSommets);
            villes = null;
        }
    }

    // Ajouter des chemins entre les villes (selon un maillage routier approximatif)
    public MapWindow initialiserLesRoutes(MapWindow mapWindow)
    {
        double distance = calculerDistance(g.get_sommets()[1],g.get_sommets()[2]);
        g.ajouterLien(1,2,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[1], g.get_sommets()[2], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[2],g.get_sommets()[14]);
        g.ajouterLien(2,14,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[2], g.get_sommets()[14], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[14],g.get_sommets()[23]);
        g.ajouterLien(14,23,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[14], g.get_sommets()[23], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[21],g.get_sommets()[23]);
        g.ajouterLien(21,23,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[21], g.get_sommets()[23], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[21],g.get_sommets()[22]);
        g.ajouterLien(21,22,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[21], g.get_sommets()[22], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[22],g.get_sommets()[13]);
        g.ajouterLien(22,13,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[22], g.get_sommets()[13], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[21],g.get_sommets()[20]);
        g.ajouterLien(21,20,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[21], g.get_sommets()[20], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[21],g.get_sommets()[4]);
        g.ajouterLien(21,4,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[21], g.get_sommets()[4], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[20],g.get_sommets()[15]);
        g.ajouterLien(20,15,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[20], g.get_sommets()[15], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[15],g.get_sommets()[4]);
        g.ajouterLien(15,4,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[15], g.get_sommets()[4], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[4],g.get_sommets()[23]);
        g.ajouterLien(4,23,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[4], g.get_sommets()[23], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[4],g.get_sommets()[18]);
        g.ajouterLien(4,18,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[4], g.get_sommets()[18], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[4],g.get_sommets()[11]);
        g.ajouterLien(4,11,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[4], g.get_sommets()[11], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[15],g.get_sommets()[11]);
        g.ajouterLien(15,11,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[15], g.get_sommets()[11], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[11],g.get_sommets()[18]);
        g.ajouterLien(11,18,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[11], g.get_sommets()[18], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[18],g.get_sommets()[16]);
        g.ajouterLien(18,16,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[18], g.get_sommets()[16], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[16],g.get_sommets()[23]);
        g.ajouterLien(16,23,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[16], g.get_sommets()[23], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[1],g.get_sommets()[16]);
        g.ajouterLien(1,16,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[1], g.get_sommets()[16], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[15],g.get_sommets()[5]);
        g.ajouterLien(15,5,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[15], g.get_sommets()[5], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[5],g.get_sommets()[11]);
        g.ajouterLien(5,11,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[5], g.get_sommets()[11], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[11],g.get_sommets()[9]);
        g.ajouterLien(11,9,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[11], g.get_sommets()[9], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[5],g.get_sommets()[9]);
        g.ajouterLien(5,9,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[5], g.get_sommets()[9], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[11],g.get_sommets()[17]);
        g.ajouterLien(11,17,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[11], g.get_sommets()[17], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[17],g.get_sommets()[12]);
        g.ajouterLien(17,12,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[17], g.get_sommets()[12], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[9],g.get_sommets()[12]);
        g.ajouterLien(9,12,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[9], g.get_sommets()[12], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[12],g.get_sommets()[3]);
        g.ajouterLien(12,3,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[12], g.get_sommets()[3], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[16],g.get_sommets()[3]);
        g.ajouterLien(16,3,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[16], g.get_sommets()[3], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[1],g.get_sommets()[3]);
        g.ajouterLien(1,3,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[1], g.get_sommets()[3], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[9],g.get_sommets()[10]);
        g.ajouterLien(9,10,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[9], g.get_sommets()[10], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[12],g.get_sommets()[19]);
        g.ajouterLien(12,19,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[12], g.get_sommets()[19], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[10],g.get_sommets()[19]);
        g.ajouterLien(10,19,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[10], g.get_sommets()[19], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[3],g.get_sommets()[19]);
        g.ajouterLien(3,19,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[3], g.get_sommets()[19], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[3],g.get_sommets()[8]);
        g.ajouterLien(3,8,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[3], g.get_sommets()[8], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[3],g.get_sommets()[6]);
        g.ajouterLien(3,6,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[3], g.get_sommets()[6], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[6],g.get_sommets()[8]);
        g.ajouterLien(6,8,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[6], g.get_sommets()[8], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[6],g.get_sommets()[7]);
        g.ajouterLien(6,7,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[6], g.get_sommets()[7], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[8],g.get_sommets()[7]);
        g.ajouterLien(8,7,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[8], g.get_sommets()[7], (int)distance,mapWindow);

        distance = calculerDistance(g.get_sommets()[19],g.get_sommets()[8]);
        g.ajouterLien(19,8,(int)distance);
        mapWindow = afficherRoute(g.get_sommets()[19], g.get_sommets()[8], (int)distance,mapWindow);

        return mapWindow;
    }

    public Sommet[] getVilles() {
        if(g != null)
        {
            return g.get_sommets();
        }
        return null;
    }

    // Méthode de la loi des sinus : http://villemin.gerard.free.fr/aGeograp/Distance.htm
    public int calculerDistance(Sommet s, Sommet t)
    {
        double d;
        d = acos(sin(toRadians(s.getY()))*sin(toRadians(t.getY())) + cos(toRadians(s.getY()))*cos(toRadians(t.getY()))*cos(toRadians(s.getX()-t.getX())))*6371;
        return (int)d;
    }

    public MapWindow initilialiserCarte(MapWindow window)
    {
        if(window != null)
        {
            // Dessin des bordures de la carte de France
            BufferedReader r = null;
            try {
                r = new BufferedReader(new FileReader(FICHIERSOURCECARTE));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String line = null;
            Point cur, prec = null;
            int readCount = 0;
            int errCount = 0;
            while(true) {
                try {
                    if (!((line = r.readLine()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                readCount++;
                Matcher m = POINT.matcher(line);
                if(m.matches()) {
                    double lon = Double.parseDouble(m.group(1));
                    double lat = Double.parseDouble(m.group(2));
                    cur = new Point(lat, lon);
                    if(prec != null) window.addSegment(new Segment(prec, cur, Color.BLUE));
                    prec = cur;
                } else errCount++;
            }
        }
        return window;
    }

    public MapWindow afficherRoute(Sommet s, Sommet t, int distance, MapWindow window)
    {
        if(g != null && window != null)
        {
            window.addSegment(new Segment(new Point(s.getY(),s.getX()),new Point(t.getY(),t.getX()),Color.BLACK));
            window.addPOI(new POI(new Point((s.getY()+t.getY())/2,(s.getX()+t.getX())/2), String.valueOf(distance)));
        }
        return window;
    }

    public MapWindow afficherSegment(Sommet s, Sommet t, MapWindow window)
    {
        if(g != null && window != null)
        {
            window.addSegment(new Segment(new Point(s.getY(),s.getX()),new Point(t.getY(),t.getX()),Color.RED));
        }
        return window;
    }

    public MapWindow afficherVilles(MapWindow window)
    {
        if(g != null && window != null)
        {
            // Ajout du maillage de villes
            for (int i = 1; i< g.get_sommets().length;i++)
            {
                window.addPOI(new POI(new Point(g.get_sommets()[i].getY(),g.get_sommets()[i].getX()), g.get_sommets()[i].getData()[0]));
            }
        }
        return window;
    }

    public Graphe getG() {
        return g;
    }
}
