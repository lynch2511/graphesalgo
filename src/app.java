import DAO.FichierDAO;
import Graphe.Graphe;
import Graphe.Sommet;
import Vues.afficheurCLI;
import Modeles.*;
import eu.jacquet80.minigeo.MapWindow;

import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;
import java.awt.Color;
import java.util.*;
import java.util.List;


public class app {

    private static Graphe gCourant;
    private static ArrayList<Algorithme> listeAlgos;
    private static afficheurCLI af;


    public static void main(String[] args)
    {
        String str="***************GRAPH-EDITOR***************\n";
        str+= "Veuillez choisir la version de ce programme : \n";
        afficheurCLI.print(str);
        List<String> opt = new ArrayList<String>() {
        };
        opt.add("2-- Mode Console");

        initAlgorithmes();

        int choix = afficheurCLI.afficherOptions(opt,2,2);

        switch (choix)
        {
            case 1: // Graphique
                break;
            case 2: // CLI
                af = new afficheurCLI();
                int n=-1;
                n = af.afficherMenu(1,null,null);
                while(n!= 0)
                {
                    switch(n)
                    {
                        case 1: // Créer un graphe
                            if(gCourant == null)
                            {
                                boolean gOriente=false;
                                boolean gPondere=false;
                                boolean gOrdonnancement = false;
                                if(af.afficherMenu(11,null,null) == 1) { gOriente = true;}
                                if(af.afficherMenu(111,null,null) == 1) {gPondere = true;}
                                if(gPondere) {if(af.afficherMenu(1111,null,null) ==1){ gOrdonnancement = true;}}
                                gCourant = new Graphe(gPondere,gOriente,gOrdonnancement);
                                af.afficherGraphe(gCourant);
                            }
                            while(n!=0) {
                                switch(n)
                                {
                                    case 11: // Créer un sommet
                                        gCourant.ajouterSommet();
                                        af.rafraichirAffichage(gCourant);
                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                        break;
                                    case 12: // Créer un lien
                                        int sDepart = af.afficherMenu(120,gCourant,null);
                                        int sArrivee = af.afficherMenu(1200,gCourant,null);
                                        int poids = -1;
                                        if(gCourant.graphePondere())
                                        {
                                            if(gCourant.grapheOrdonnancement())
                                            {
                                                if(!gCourant.PossedeSuccesseur(sDepart))
                                                {
                                                    // Demander de saisir la durée de la tâche
                                                    ArrayList<Integer> d = new ArrayList<Integer>();
                                                    d.add(sDepart);
                                                    poids = af.afficherMenu(12001,null,d);
                                                }
                                                else
                                                {
                                                    poids = gCourant.get_sommets()[sDepart].getDuree();
                                                    afficheurCLI.print("La durée de "+sDepart+" est "+poids+"\n");
                                                }
                                            }
                                            else
                                            {
                                                // Demander de saisir le poids
                                                poids = af.afficherMenu(12000,null,null);
                                            }
                                        }
                                        if(!gCourant.ajouterLien(sDepart,sArrivee, poids)){afficheurCLI.print("Lien déjà existant!\n");}
                                        af.rafraichirAffichage(gCourant);
                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                        break;
                                    case 13: // Supprimer un sommet
                                        int sommet = af.afficherMenu(14,gCourant,null);
                                        if(!gCourant.supprimerSommet(sommet)){
                                            afficheurCLI.print("Sommet introuvable.");
                                        }
                                        else
                                        {
                                            af.rafraichirAffichage(gCourant);
                                        }
                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                        break;
                                    case 14: // Supprimer un lien
                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                        int s = af.afficherMenu(120,gCourant,null);
                                        int t = af.afficherMenu(1200,gCourant,null);
                                        if(!gCourant.supprimerLien(s,t))
                                        {
                                            afficheurCLI.print("Lien introuvable");
                                        }
                                        else
                                        {
                                            af.rafraichirAffichage(gCourant);
                                        }
                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                        break;
                                    case 15: // Sauvegarder
                                        n = af.afficherMenu(150,gCourant,null);
                                        String chemFic= afficheurCLI.afficherSaisieFichier();
                                        switch(n)
                                        {
                                            case 1: // codage prufer
                                                if(!FichierDAO.ecrireGrapheFichier(chemFic,gCourant, FichierDAO.formats.PRUFER,listeAlgos)) {afficheurCLI.print("Erreur lors de la sauvegarde\n");} else {afficheurCLI.print("Enregistrement effectué\n");System.exit(0);}
                                                break;
                                            case 2: // FS/APS
                                                if(!FichierDAO.ecrireGrapheFichier(chemFic,gCourant,FichierDAO.formats.FSAPS, listeAlgos)) {afficheurCLI.print("Erreur lors de la sauvegarde\n");} else {afficheurCLI.print("Enregistrement effectué\n");System.exit(0);}
                                                break;
                                        }

                                        break;
                                    case 16: // Appliquer un algorithme
                                        n = af.afficherMenu(160,gCourant,listeAlgos);
                                        switch(n)
                                        {
                                            case 1: // Algorithme du Rang
                                                gCourant.poidsADJ_TO_FSAPS();
                                                AlgoRang ar = new AlgoRang(listeAlgos.get(1).getNom(),listeAlgos.get(1).getDescription(),gCourant.get_fs(),gCourant.get_aps());
                                                int[] resRang = ar.rang();
                                                af.afficherTab(resRang);
                                                af.afficherRangs(resRang);

                                                // sauvegarde du résultat
                                                int[][] resSauv = new int[0][0];
                                                resSauv = ArrayUtils.add(resSauv,resRang);
                                                if(FichierDAO.sauvegarderResultat(resSauv,ar)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir")+"\n");}
                                                resSauv = null;
                                                break;
                                            case 2: // Algorithme de dijkstra
                                                gCourant.poidsADJ_TO_FSAPS();
                                                int sommetDepart = af.afficherMenu(16020,gCourant,null);
                                                int sommetArrivee = af.afficherMenu(160200,gCourant,null);
                                                AlgoDijkstra ad = new AlgoDijkstra(listeAlgos.get(2).getNom(),listeAlgos.get(2).getDescription(),gCourant.get_fs(),gCourant.get_aps(),gCourant.getMatricePoids(),sommetDepart);
                                                int[][] res =ad.Dijkstra();
                                                int[] pcchemin = new int[1]; // stocker en 0 la distance de ce chemin
                                                pcchemin=ArrayUtils.add(pcchemin,sommetArrivee);
                                                // chercher si le sommet d'arrivée est atteignable dans le tableau des distances
                                                int k = sommetArrivee;
                                                if(res[0][sommetArrivee] != Integer.MAX_VALUE)
                                                {
                                                    while(res[1][k]>= 0)
                                                    {
                                                        k = res[1][k];  // pred
                                                        pcchemin =ArrayUtils.add(pcchemin,k);
                                                    }
                                                }
                                                pcchemin=ArrayUtils.add(pcchemin,sommetDepart);
                                                afficheurCLI.print("\n Le plus court chemin est (du sommet d'arrivée au sommet de départ): \n");
                                                af.afficherTab(pcchemin);

                                                // sauvegarde du résultat
                                                int[][] resSauv2 = new int[0][0];
                                                resSauv2 = ArrayUtils.add(resSauv2,pcchemin);
                                                if(FichierDAO.sauvegarderResultat(resSauv2,ad)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir"));}
                                                resSauv2 = null;

                                                if(res[0][sommetArrivee] != Integer.MAX_VALUE)
                                                {
                                                    afficheurCLI.print(" de taille "+ res[0][sommetArrivee]+"\n");
                                                }
                                                else
                                                {
                                                    afficheurCLI.print(" est inatteignable ! \n");
                                                }

                                                break;
                                            case 3: // codage de prufer
                                                n = af.afficherMenu(1610,gCourant,null);
                                                AlgoPrufer ap = new AlgoPrufer(listeAlgos.get(3).getNom(),listeAlgos.get(3).getDescription(),gCourant.get_poidsAdjacence());
                                                switch(n)
                                                {
                                                    case 1: // Décodage
                                                       int[] saisie = af.afficherSaisieTab(16100);

                                                        gCourant.set_poidsAdjacence(ap.decodage(saisie));
                                                        Sommet[] ss = new Sommet[1];
                                                        ss[0] = new Sommet(0); // sommet fictif
                                                        for(int i = 1; i<= gCourant.nbSommets();i++)
                                                        {
                                                            Sommet sn = new Sommet(i);
                                                            ss=ArrayUtils.add(ss,sn);
                                                        }
                                                        gCourant.set_sommets(ss);
                                                        af.rafraichirAffichage(gCourant);
                                                        af.afficherMat(gCourant.get_poidsAdjacence());
                                                        // sauvegarde du résultat
                                                        if(FichierDAO.sauvegarderResultat(gCourant.get_poidsAdjacence(),ap)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir"));}
                                                        break;
                                                    case 2: // codage
                                                        int[] resPrufer = ap.CodageAlgoPrufer();
                                                        // sauvegarde du résultat
                                                        int[][] resSauv3 = new int[0][0];
                                                        resSauv3 = ArrayUtils.add(resSauv3,resPrufer);
                                                        if(FichierDAO.sauvegarderResultat(resSauv3,ap)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir"));}
                                                        resSauv3 = null;
                                                        af.afficherTab(resPrufer);
                                                        break;
                                                }
                                                break;
                                            case 4: // Algorithme de kruskal
                                                AlgoKruskal ak = new AlgoKruskal(listeAlgos.get(4).getNom(),listeAlgos.get(4).getDescription(),gCourant);
                                                Graphe gk = ak.Kruskal();
                                                af.rafraichirColoration(gk, Color.MAGENTA);
                                                af.afficherMat(gk.get_poidsAdjacence());
                                                // sauvegarde du résultat
                                                if(FichierDAO.sauvegarderResultat(gk.get_poidsAdjacence(),ak)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir"));}
                                                n = af.afficherMenu(1630,gCourant,null);
                                                switch(n)
                                                {
                                                    case 1:
                                                        af.rafraichirColoration(gCourant,null);
                                                        break;

                                                }
                                                break;
                                            case 5: // Algorithme d'ordonnancement
                                                AlgoOrdonnancement ao = new AlgoOrdonnancement(listeAlgos.get(5).getNom(),listeAlgos.get(5).getDescription(),gCourant);
                                                gCourant.poidsADJ_TO_FSAPS();
                                                int[] ddi = ao.det_ddi();
                                                if(ddi != null)
                                                {
                                                    int[] app = ao.det_app(ddi);
                                                    if(app!= null)
                                                    {
                                                        int[] fp = ao.det_fp(app);
                                                        if(fp != null)
                                                        {
                                                            int[] d = ao.det_d();
                                                            ao.ordonnancement(fp,app,d);
                                                            afficheurCLI.print("Tableau appc :\n");
                                                            af.afficherTab(ao.get_appc());
                                                            afficheurCLI.print("Tableau fpc :\n");
                                                            af.afficherTab(ao.get_fpc());
                                                            afficheurCLI.print("Tableau lc :\n");
                                                            af.afficherTab(ao.get_lc());

                                                            af.afficherLC(ao.get_lc());
                                                            // sauvegarde du résultat
                                                            int[][] resSauv4 = new int[0][0];
                                                            resSauv4 = ArrayUtils.add(resSauv4,ao.get_appc());
                                                            resSauv4 = ArrayUtils.add(resSauv4,ao.get_fpc());
                                                            resSauv4 = ArrayUtils.add(resSauv4,ao.get_lc());
                                                            if(FichierDAO.sauvegarderResultat(resSauv4,ao)){afficheurCLI.print("résultat sauvegardé dans : "+System.getProperty("user.dir"));}
                                                            resSauv4 = null;
                                                        }
                                                        else
                                                        {
                                                            afficheurCLI.print("fp null\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        afficheurCLI.print("ddi null\n");
                                                    }
                                                }
                                                else
                                                {
                                                    afficheurCLI.print("fs et aps null\n");
                                                }
                                                break;
                                            case 6: // Tarjan
                                                AlgoTarjan at = new AlgoTarjan(listeAlgos.get(6).getNom(),listeAlgos.get(6).getDescription());
                                                gCourant.poidsADJ_TO_FSAPS();
                                                at.fortconnexe(gCourant.get_aps(), gCourant.get_fs());
                                                int[] cfc = at.getCfc();
                                                af.afficherTab(cfc);
                                                at.graph_reduit(at.getPrem(),at.getPilch(),at.getCfc(), gCourant.get_fs(), gCourant.get_aps());
                                                int[] fsr = at.getFsr();
                                                af.afficherTab(fsr);
                                                int[] apsr = at.getApsr();
                                                af.afficherTab(apsr);
                                                break;
                                            case 7: // algorithme de Dantzig
                                                AlgoDantzig adz = new AlgoDantzig(listeAlgos.get(7).getNom(),listeAlgos.get(7).getDescription());
                                                adz.Dantzig(gCourant.get_poidsAdjacence());
                                                af.afficherMat(adz.getMatCoutsDistances());
                                                break;
                                        }
                                        break;
                                }
                                n = af.afficherMenu(10,gCourant,null);
                            }
                            break;
                        case 2: // Importer un graphe
                                int[][] res = FichierDAO.lectureFichier(afficheurCLI.afficherSaisieFichier());
                                if(res != null &&  res[0] != null && res[0][0] != 0 && res[0].length == 4)
                                {
                                    boolean oriente = false;
                                    boolean pondere = false;
                                    boolean ordonnancement = false;
                                    if(res[0][1] == 1) // oriente
                                    {
                                        oriente = true;
                                    }
                                    if(res[0][2] == 1)
                                    {
                                        pondere = true;
                                    }
                                    if(res[0][3] ==1 && pondere)
                                    {
                                        ordonnancement = true;
                                    }
                                    switch(res[0][0])
                                    {
                                        case 1: // enregistré avec le codage de prufer
                                            if(res[1] != null && !oriente && !pondere && !ordonnancement)
                                            {
                                                String chemFic="";
                                                AlgoPrufer ap = new AlgoPrufer(listeAlgos.get(3).getNom(),listeAlgos.get(3).getDescription());
                                                gCourant = new Graphe(false,false,false);
                                                gCourant.set_poidsAdjacence(ap.decodage(res[1]));
                                                // Ajout des sommets au graphe
                                                gCourant.initSommets(gCourant.get_poidsAdjacence()[0][0]);

                                                af.afficherGraphe(gCourant);
                                            }
                                            else
                                            {
                                                afficheurCLI.print("Fichier mal formaté");
                                            }
                                            break;
                                        case 2: // enregistré avec FS/APS
                                            if(res[1] != null && res[2] != null)
                                            {
                                                int nbSommets = res[2][0]; // dans aps
                                                gCourant = new Graphe(pondere,oriente,ordonnancement);
                                                // ajout des sommets dans le tableau de sommets
                                                gCourant.initSommets(nbSommets);
                                                if(pondere && res[3] != null)
                                                {
                                                    // reconstruire la matrice des poids
                                                    int[][] matpoids = new int[0][0];
                                                    for (int i = 3;i<nbSommets+4;i++)
                                                    {
                                                        if(res[i] != null)
                                                        {
                                                            matpoids = ArrayUtils.add(matpoids,res[i]);
                                                        }
                                                    }
                                                    gCourant.FSAPS_TO_MATADJPOIDS(res[1],res[2],matpoids);
                                                }
                                                else
                                                {
                                                    gCourant.FSAPS_TO_MATADJPOIDS(res[1],res[2],null);
                                                }
                                                // affichage du graphe importé
                                                af.afficherGraphe(gCourant);
                                            }
                                            else
                                            {
                                                afficheurCLI.print("Fichier mal formaté\n");
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    afficheurCLI.print("Erreur lors de la lecture de ce fichier!\n");
                                }
                            break;
                        case 5: // Quitter
                            System.exit(0);
                            break;
                        case 4: // application

                            ApplicationExemple aex = new ApplicationExemple(24);
                            aex.initialiserVilles();
                            MapWindow window = new MapWindow();
                            window = aex.initilialiserCarte(window);
                            window = aex.afficherVilles(window);
                            window = aex.initialiserLesRoutes(window);
                            window.setLocationRelativeTo(null);
                            window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                            window.setVisible(true);

                            // Demander la ville de départ et d'arrivée
                            int villeDepart = af.afficherMenu(40,aex.getG(),null);
                            int villeArrivee = af.afficherMenu(400,aex.getG(),null);
                            // Calculer et afficher le plus court chemin calculé par dijkstra
                            aex.getG().poidsADJ_TO_FSAPS();
                            AlgoDijkstra ad = new AlgoDijkstra(listeAlgos.get(2).getNom(),listeAlgos.get(2).getDescription(),aex.getG().get_fs(),aex.getG().get_aps(),aex.getG().getMatricePoids(),villeDepart);
                            int[][] resd =ad.Dijkstra();
                            int[] pcchemin = new int[1]; // stocker en 0 la distance de ce chemin
                            pcchemin=ArrayUtils.add(pcchemin,villeArrivee);
                            // chercher si le sommet d'arrivée est atteignable dans le tableau des distances
                            int k = villeArrivee;
                            if(resd[0][villeArrivee] != Integer.MAX_VALUE)
                            {
                                while(resd[1][k]>= 0)
                                {
                                    k = resd[1][k];  // pred
                                    pcchemin =ArrayUtils.add(pcchemin,k);
                                }
                            }
                            pcchemin=ArrayUtils.add(pcchemin,villeDepart);
                            int distancetot = 0;
                            for(int i = pcchemin.length-1; i>1;i--)
                            {
                                window = aex.afficherSegment(aex.getG().get_sommets()[pcchemin[i]], aex.getG().get_sommets()[pcchemin[i-1]],window);
                                distancetot+= aex.getG().getPoids(pcchemin[i],pcchemin[i-1]);
                            }
                            afficheurCLI.print("Distance minimale totale : "+distancetot + " kilomètres.\n");
                            window.repaint();
                            break;

                        case 3: // Afficher les informations d'un algorithme
                            for(Algorithme a : listeAlgos)
                            {
                                if(a.getNom() != "0")
                                {
                                    afficheurCLI.print(a.getNom()+" : "+a.getDescription()+"\n");
                                }
                            }
                            break;
                    }
                    if(gCourant!=null && n!= 0)
                    {
                        n = 1; // relancer sur le menu après création de graphe
                    }
                    else
                    {
                        n = af.afficherMenu(1,null,null);
                    }
                }

        }
    }


    public static void initAlgorithmes()
    {
        listeAlgos = new ArrayList<Algorithme>();
        listeAlgos.add(new Algorithme("0","0"));
        AlgoRang ar = new AlgoRang("Algorithme du Rang", "Algorithme permettant de déterminer le rang de chaque sommet dans un graphe orienté ");
        AlgoDijkstra ad = new AlgoDijkstra("Algorithme de Dijkstra", "Algorithme permettant de déterminer un plus court chemin pour des poids non négatifs");
        AlgoPrufer ap = new AlgoPrufer("Algorithme de Prüfer", "Algorithme permettant de déterminer le code d'un graphe non orienté et non valué");
        AlgoKruskal ak = new AlgoKruskal("Algorithme de Kruskal", "Algorithme permettant de déterminer un graphe réduit");
        AlgoOrdonnancement ao = new AlgoOrdonnancement("Algorithme d'ordonnancement", "Algorithme permettant de déterminer les chemins critiques des tâches d'un projet");
        AlgoTarjan at = new AlgoTarjan("Algorithme de Tarjan","Algorithme permettant de déterminer les composantes fortements connexes d'un graphe orienté.");
        AlgoDantzig adz = new AlgoDantzig("Algorithme de Dantzig", "Algorithme permettant de déterminer un plus court chemin pour des poids positifs ou nuls");

        listeAlgos.add(ar);
        listeAlgos.add(ad);
        listeAlgos.add(ap);
        listeAlgos.add(ak);
        listeAlgos.add(ao);
        listeAlgos.add(at);
        listeAlgos.add(adz);
    }
}
