package Graphe;

public class Arete {
    private int sommetArrivee;
    private int sommetDepart;
    double poids;

    public Arete(int sommetArrivee, int sommetDepart) {
        this.sommetArrivee = sommetArrivee;
        this.sommetDepart = sommetDepart;
    }

    public int getSommetArrivee() {
        return sommetArrivee;
    }

    public int getSommetDepart() {
        return sommetDepart;
    }

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }

    public Arete(int sommetArrivee, int sommetDepart, double poids) {
        this.sommetArrivee = sommetArrivee;
        this.sommetDepart = sommetDepart;
        this.poids = poids;
    }
}
