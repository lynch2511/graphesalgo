package Graphe;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;

public class Graphe {
    private boolean _graphePondere = false;
    private boolean _grapheOriente = false;
    private boolean _grapheOrdonnancement = false;
    private int[] _fs;
    private int[] _aps;
    private int[][] _poidsAdjacence;
    private int[][] matricePoids;
    private int[] _rang;
    private Sommet[] _sommets;
    private ArrayList<Arete> _aretes;

    public int[][] getMatricePoids() {
        return matricePoids;
    }

    public void setMatricePoids(int[][] matricePoids) {
        this.matricePoids = matricePoids;
    }

    public Graphe(boolean value, boolean oriente, boolean ordonnancement) {
        this._graphePondere = value;
        this._grapheOriente = oriente;
        this._grapheOrdonnancement = ordonnancement;
        this._sommets = new Sommet[0];
    }

    public Graphe(Graphe g) {
        this._grapheOriente = g.grapheOriente();
        this._graphePondere = g.graphePondere();
        _aretes = new ArrayList<Arete>();
    }

    /***
     * Transforme une matrice de poidsAdjacence en tableaux FS et APS et matrice de poids si pondéré
     */
    public void poidsADJ_TO_FSAPS()
    {
        if(_poidsAdjacence != null)
        {
            int nbSommets = _poidsAdjacence[0][0];
            int nbLiens = _poidsAdjacence[0][1];
            _aps = new int[nbSommets+1];
            _fs = new int[(nbLiens*2)+nbSommets+1];
            matricePoids = new int[nbSommets+1][nbSommets+1];
            for(int i = 1; i<= nbSommets;i++)
            {
                for(int j = 1; j<= nbSommets;j++)
                {
                    matricePoids[i][j] = Integer.MAX_VALUE; // initialiser a inf lorsqu'il n'y a pas de liens
                }
            }
            _aps[0] = nbSommets;
            _fs[0] = nbSommets+nbLiens;
            int k = 1;
            for(int i = 1; i<= nbSommets; i++)
            {
                _aps[i] = k;// adresse du premier successeur de i
                for(int j = 1; j<= nbSommets;j++)
                {
                    if(_poidsAdjacence[i][j] != Integer.MAX_VALUE) // si il y a un lien (arete ou arc)
                    {
                        //_fs= ArrayUtils.add(_fs,k,j);
                        _fs[k++]=j;
                        if(_graphePondere)
                        {
                            this.matricePoids[i][j] = _poidsAdjacence[i][j];
                        }
                    }
                }
                _fs[k] = 0;
                k++;
            }
        }
    }

    /***
     * Transforme Deux tableaux FS APS et matrice de poids adjacence (optionnel) en matricePoidsAdjacence
     */
    public void FSAPS_TO_MATADJPOIDS(int[] fs, int[] aps, int[][] matPoidsOptionnelle)
    {
        int j;
        int nbSommets = aps[0];
        int nbLiens = fs[0]-nbSommets;
        this._poidsAdjacence = new int[nbSommets+1][nbSommets+1];
        // Construction de la matrice
        for (int i=0; i<= nbSommets; i++)
        {
            this._poidsAdjacence[i] = new int[nbSommets+1];
        }

        // installer le nb de sommets et de liens dans la matrice
        this._poidsAdjacence[0][0] = nbSommets;
        this._poidsAdjacence[0][1] = nbLiens;

        // Installer les 0 (ici des -1 car il s'agit d'une matrice poidsAdjacence dans la matrice
        for(int i = 1; i<= nbSommets; i++)
        {
            for(j = 1; j<= nbSommets; j++)
            {
                this._poidsAdjacence[i][j] = Integer.MAX_VALUE;
            }
        }
        // Installer les données dans la matrice
        for (int i = 1; i<= nbSommets;i++)
        {
            int k = aps[i];
            while(fs[k] != 0)
            {
                if(matPoidsOptionnelle!= null) // le graphe est valué
                {
                    int p = matPoidsOptionnelle[i][fs[k]];
                    this._poidsAdjacence[i][fs[k]] = p;
                    if(_grapheOrdonnancement && _sommets[i].getDuree() == -1)
                    {
                        // mettre à jour la durée du sommet/tache de départ
                        _sommets[i].setDuree(p);
                    }
                }
                else
                {
                    this._poidsAdjacence[i][fs[k]] = 1; // lien entre i,k marqué par un 0 dans la matrice de poidsAdjacence
                }

                k++;
            }
        }
    }

    public void tabAretes2MatAdj(int nbSommets)
    {
        _poidsAdjacence = new int[nbSommets+1][nbSommets+1];
        _sommets = new Sommet[nbSommets+1];
        _sommets[0] = new Sommet(0);

        int j,k;
        // Construction de la matrice
        for (int i=0; i<= nbSommets; i++)
        {
            _poidsAdjacence[i] = new int[nbSommets+1];
            if(i>0)
            {
                _sommets[i] = new Sommet(i);
            }
        }

        // installer le nb de sommets et de liens dans la matrice
        _poidsAdjacence[0][0] = nbSommets;
        _poidsAdjacence[0][1] = _aretes.size();

        // Installer les 0 (ici des -1 car il s'agit d'une matrice poidsAdjacence dans la matrice
        for(int i = 1; i<= nbSommets; i++)
        {
            for(j = 1; j<= nbSommets; j++)
            {
                _poidsAdjacence[i][j] = Integer.MAX_VALUE;
            }
        }
        // Installer les données dans la matrice
        for (int i = 0; i< _aretes.size();i++)
        {
            _poidsAdjacence[_aretes.get(i).getSommetDepart()][_aretes.get(i).getSommetArrivee()] = (int)_aretes.get(i).getPoids();
            _poidsAdjacence[_aretes.get(i).getSommetArrivee()][_aretes.get(i).getSommetDepart()] = (int)_aretes.get(i).getPoids();
            _poidsAdjacence[_aretes.get(i).getSommetArrivee()][0]++;
        }
    }

    public void matPoidsAdj2Aretes()
    {
        _aretes = new ArrayList<Arete>();
        for(int i = 1; i<= nbSommets() ; i++)
        {
            if(_poidsAdjacence[i][0] > 0)
            {
                for(int j = 1; j<= nbSommets(); j++)
                {
                    int p = _poidsAdjacence[i][j];
                    if(p != Integer.MAX_VALUE && j > i)
                    {
                        _aretes.add(new Arete(i,j,p));
                    }
                }
            }
        }
    }

    public void trierAretes()
    {
        if(_aretes != null)
        {
            double p;
            for (int i = 0; i < nbLiens() - 1; i++)
                for (int j = i + 1; j < nbLiens(); j++)
                    if ((_aretes.get(j).getPoids() < _aretes.get(i).getPoids()) || (_aretes.get(j).getPoids() == _aretes.get(i).getPoids() && _aretes.get(j).getSommetDepart() < _aretes.get(i).getSommetArrivee()) || (_aretes.get(j).getPoids() == _aretes.get(i).getPoids() && _aretes.get(j).getSommetArrivee() < _aretes.get(i).getSommetArrivee()))
                    {
                        p = _aretes.get(j).getPoids();
                        _aretes.get(j).setPoids(_aretes.get(i).getPoids());
                        _aretes.get(i).setPoids(p);
                    }
        }
    }

    public int nbSommets()
    {
        if(_sommets != null && _sommets.length > 0) {
                return _sommets.length - 1;
             // le sommet à l'indice 0 est fictif
        }
        else if(_poidsAdjacence != null)
        {
            return _poidsAdjacence[0][0];
        }
        return 0;
    }

    public int nbLiens()
    {
        //int nb = 0;
        //for(int i = 0; i< this.poidsAdjacence.length;i++)
        //{
        //    nb += this.poidsAdjacence[i][0]; // le nb de liens est stocké en colonne 0 pour chaque ligne
        //}
        if(_poidsAdjacence != null)
        {
            return _poidsAdjacence[0][1];
        }
        return  _aretes.size();
    }

    public boolean supprimerLien(int s, int t)
    {
        if(this._poidsAdjacence[s][t] == Integer.MAX_VALUE) {return false;} // non existant dans tous les cas
        if(_grapheOriente)
        {
            this._poidsAdjacence[s][t] = Integer.MAX_VALUE;
            // mise à jour du nb de lien pour la ligne du sommet s
            this._poidsAdjacence[s][0] = this._poidsAdjacence[s][0]-1;
            this._poidsAdjacence[0][1] = this._poidsAdjacence[0][1]-1;
        }
        else
        {
            this._poidsAdjacence[s][t] = Integer.MAX_VALUE;
            this._poidsAdjacence[t][s] = Integer.MAX_VALUE;
            // mise à jour du nb de lien pour la ligne du sommet s
            this._poidsAdjacence[s][0] = this._poidsAdjacence[s][0]-1;
            this._poidsAdjacence[0][1] = this._poidsAdjacence[0][1]-1;
        }
        return true;
    }

    public Sommet[] get_sommets() {
        return _sommets;
    }

    public void set_sommets(Sommet[] _sommets) {
        this._sommets = _sommets;
    }

    public void initSommets(int nbSommets)
    {
        _sommets=new Sommet[nbSommets+1];
        _sommets[0] = new Sommet(0);
        for(int i = 1; i<= nbSommets;i++)
        {
            _sommets[i] = new Sommet(i);
        }
    }

    public boolean lienExistant(int s, int t)
    {
        if(_poidsAdjacence != null)
        {
            if(_poidsAdjacence[s][t] != Integer.MAX_VALUE)
            {
                return true;
            }
        }
        return false;
    }

    public boolean PossedeSuccesseur(int s)
    {
        poidsADJ_TO_FSAPS();
        if(_fs[_aps[s]]!=0) {
            // s a au moins un successeur
            return true;
        }
        return false;
    }

    public int getPoids(int s,int t)
    {
        if(_grapheOriente)
        {
            return _poidsAdjacence[s][t];
        }
        else
        {
            if(_poidsAdjacence[s][t] != Integer.MAX_VALUE)
            {
                return  _poidsAdjacence[s][t];
            }
            else
            {
                return  _poidsAdjacence[t][s];
            }
        }
    }

    public boolean ajouterLien(int s, int t, int optionalPoids)
    {
        if(!lienExistant(s,t)) // lien inexistant
        {
            if(optionalPoids != -1) // PONDERE
            {
                if(_grapheOriente)
                {
                    this._poidsAdjacence[s][t] = optionalPoids;
                }
                else
                {
                    this._poidsAdjacence[s][t] = optionalPoids;
                    this._poidsAdjacence[t][s] = optionalPoids;
                }
                if(_grapheOrdonnancement)
                {
                    // mettre à jour la durée du sommet/tache de départ
                    _sommets[s].setDuree(optionalPoids);
                }
            }
            else                // NON-PONDERE
            {
                if(_grapheOriente)
                {
                    this._poidsAdjacence[s][t] = 1; // lien de valeur 1 = non PONDERE
                }
                else
                {
                    this._poidsAdjacence[s][t] = 1;
                    this._poidsAdjacence[t][s] = 1;
                }

            }
            // mettre à jour le nb de lien dans la ligne de s (qu'il soit orienté ou non, pondéré ou non)
            this._poidsAdjacence[s][0]++;
            this._poidsAdjacence[0][1]++;
            return true;
        }
        return false;
    }

    public void initialiserMatPoidsAdj(int nbSommets)
    {
        _poidsAdjacence = new int[nbSommets+1][nbSommets+1];
        // initialiser la matrice du graphe
        for(int i = 0;i<=nbSommets;i++)
        {
            for(int j = 0; j<=nbSommets;j++)
            {
                if(i == 0 || j == 0)
                {
                    _poidsAdjacence[i][j] =0;
                }else {
                    _poidsAdjacence[i][j]=Integer.MAX_VALUE;
                }
            }
        }
        _poidsAdjacence[0][0] = nbSommets;
    }

    public boolean ajouterSommet()
    {
        int ID = _sommets.length;
        if(_sommets.length ==0)
        {
            ID=1;
            _sommets = ArrayUtils.add(_sommets, new Sommet(0));// sommet fictif à indice 0
        }
        _sommets = ArrayUtils.add(_sommets,new Sommet(ID));

        // maj de la matrice d'poidsAdj,
        if(_poidsAdjacence == null) {
            _poidsAdjacence = new int[1][1];
        }
        // ajout de la ligne du nouveau sommet
        int[] nl = new int[_sommets.length];
        for(int i = 0;i<=ID;i++)
        {
            if(i == 0)
            {
                nl[i] =0;
            }else {
                nl[i]=Integer.MAX_VALUE;
            }
        }
        _poidsAdjacence = ArrayUtils.add(_poidsAdjacence,nl);
        // Ajout de la colonne du nouveau sommet sur les lignes des autres sommets
        for(int i = 0;i<ID;i++)
        {
            if(i==0)
            {
                _poidsAdjacence[i] = ArrayUtils.add(_poidsAdjacence[i],0);
            }
            else
            {
                _poidsAdjacence[i] = ArrayUtils.add(_poidsAdjacence[i],Integer.MAX_VALUE);
            }
        }
        // Mise à jour du nobmre de sommets total
        _poidsAdjacence[0][0]+=1;
        return true;
    }

    public boolean supprimerSommet(int ID)
    {
        if (_sommets[ID].getID()==ID && ID != 0) // le sommet à l'indice ID a bien l'ID = ID (existe)
        {
            _sommets = ArrayUtils.remove(_sommets,ID);
            for (int i = 0; i< _poidsAdjacence.length; i++)
            {
                if(_poidsAdjacence[i][ID] != Integer.MAX_VALUE && i != 0)
                {
                    // décrémenter le nb de liens sur cette ligne de 1
                    _poidsAdjacence[i][0]--;
                    _poidsAdjacence[0][1]--;
                }
                _poidsAdjacence[i] = ArrayUtils.remove(_poidsAdjacence[i],ID);
            }
            _poidsAdjacence = ArrayUtils.remove(_poidsAdjacence,ID);
            // mettre à jour les numéros des autres sommets (décrémenter de 1 les suivants)
            for(int i = ID; i< _sommets.length; i++)
            {
                _sommets[i].setID(_sommets[i].getID()-1);
            }
            // mettre à jour le nb de sommets total
            _poidsAdjacence[0][0]--;
            return true;
        }
        return false;
    }

    public boolean algoApplicable(int j)
    {
        switch (j)
        {
            case 1: // Algo du rang
                if (_grapheOriente && !circuits()) {return true;}
                break;
            case 2: // Dijkstra
                if(_graphePondere && poidsNonNuls() && !_grapheOrdonnancement) {return true;}
                break;
            case 3: // Prufer
                if(!_graphePondere && !_grapheOriente && !_grapheOrdonnancement && nbSommets() >= 3) {return true;}
                break;
            case 4: // Kruskal
                if(_graphePondere && !_grapheOriente) {return true;}
                break;
            case 5: // Ordonnancement
                if(_graphePondere && nbSommets() > 2 && nbLiens() >= 1 && _grapheOrdonnancement) {return true;}
                    break;
            case 6: // Tarjan et composantes fc
                if(_grapheOriente && !graphePondere() && nbSommets() > 2 && nbLiens() >=1) {return true;}
                break;
            case 7: // Dantzig
                if(_graphePondere && !_grapheOrdonnancement) {return true;}
                break;
        }
        return false;
    }

    public boolean poidsNonNuls()
    {
        for(int i = 1; i<= nbSommets(); i++)
        {
            for (int j =1; j<= nbSommets(); j++)
            {
                if(_poidsAdjacence[i][j] < 0)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean graphePondere()
    {
        return this._graphePondere;
    }

    public boolean circuits()
    {
        return false;
    }

    public boolean grapheOriente()
    {
        return this._grapheOriente;
    }

    public boolean sauvegarder(int choixStructure, String path)
    {
        return true;
    }

    public ArrayList<Arete> get_aretes() {
        return _aretes;
    }

    public void set_aretes(ArrayList<Arete> _aretes) {
        this._aretes = _aretes;
    }

    public int[] get_fs() {
        return _fs;
    }

    public void set_fs(int[] _fs) {
        this._fs = _fs;
    }

    public int[] get_aps() {
        return _aps;
    }

    public void set_aps(int[] _aps) {
        this._aps = _aps;
    }

    public int[][] get_poidsAdjacence() {
        return _poidsAdjacence;
    }

    public void set_poidsAdjacence(int[][] _poidsAdjacence) {
        this._poidsAdjacence = _poidsAdjacence;
    }

    public int[] get_rang() {
        return _rang;
    }

    public void set_rang(int[] _rang) {
        this._rang = _rang;
    }

    public boolean grapheOrdonnancement() {
        return _grapheOrdonnancement;
    }
}
