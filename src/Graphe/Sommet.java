package Graphe;

public class Sommet {

    private int ID;
    private double x;
    private double y;
    private int Duree;
    private String[] data;

    public double getX() {
        return x;
    }

    public int getDuree() {
        return Duree;
    }

    public void setDuree(int duree) {
        Duree = duree;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    public Sommet(int ID) {
        this.ID = ID;
        this.setY(0.0);
        this.setX(0.0);
        this.setDuree(-1);
    }

    public Sommet(int ID, double x, double y, String nom)
    {
        this.ID = ID;
        this.setX(x);
        this.setY(y);
        this.setDuree(-1);
        this.data = new String[1];
        data[0] = nom;
    }

    public boolean superposition(double px, double py, int rayonSommets)
    {
            if(Math.abs(x-px) <= rayonSommets && Math.abs(y-py) <= rayonSommets)
            {
                return true;
            }
        return false;
    }

    @Override
    public String toString() {
        return "Sommet :" + ID;
    }


}
