package Vues;

import Graphe.Graphe;

import java.awt.*;
import javax.swing.*;

public class Surface extends JPanel {

    private Graphe _gr;
    private int[] _longueurCritiques;
    private int _largeurPlan;
    private int _hauteurPlan;
    private int _rayonSommet;
    private Color _defautColor;
    private boolean _rafraichir = false;
    Dimension FLECHE;
    private Color _couleurLiens;
    private int[] _rangs;
    public Surface(Graphe g, int largeurPlan, int hauteurPlan)
    {
        _gr=g;
        _largeurPlan = largeurPlan;
        _hauteurPlan = hauteurPlan;
        FLECHE = new Dimension(12,4);
        _rayonSommet = 28;
        _defautColor = Color.BLUE;
    }

    public void rafraichir(Graphe g,Color c)
    {
        _gr = g; // mettre à jour les données de la vue
        _couleurLiens = null;
        if(c != null) {
            _couleurLiens = c;}
    }

    public void afficherRangs(int[] rg) {_rangs = rg;}

    public void afficherLC(int[] lc)
    {
        _longueurCritiques = lc;
    }

    public void paintComponent (Graphics g)
    {
        super.paintComponent(g);
        setBackground(Color.WHITE);
        // Editer les sommets
        for(int i = 1; i<=_gr.nbSommets();i++)
        {
            double Xi;
            double Yi;
            setForeground(Color.BLUE); // couleur par défaut pour les sommets
            if(_gr.get_sommets()[i].getX() == 0 || _rafraichir) // pas de position affectée ou rafrachir les positions
            {
                // Positionner le nouveau sommet de façon aléatoire dans le plan
                Xi =  (Math.random()*(_largeurPlan-30)) - 30; // donner une bordure de -30px
                Yi =  (Math.random()*(_hauteurPlan-30)) - 30;
                for(int u = 1; u<= _gr.nbSommets(); u++)
                {
                    while(_gr.get_sommets()[u].superposition(Xi,Yi,_rayonSommet))
                    {
                        Xi =  (Math.random()*(_largeurPlan-30)) - 30; // donner une bordure de -30px
                        Yi =  (Math.random()*(_hauteurPlan-30)) - 30;
                    }
                }
                if(Xi <= _rayonSommet) { Xi+= _rayonSommet;} // donner une bordure de +30px
                if(Yi <= _rayonSommet) { Yi+= _rayonSommet;}
                _gr.get_sommets()[i].setX(Xi);
                _gr.get_sommets()[i].setY(Yi);
            }
            else
            {
                Xi = _gr.get_sommets()[i].getX();
                Yi= _gr.get_sommets()[i].getY();
            }

            // Dessin d'un cercle pour matérialiser le sommet, avec son numéro à l'intérieur
            g.drawOval((int)Xi,(int)Yi,_rayonSommet,_rayonSommet);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
            g.drawString(Integer.toString(_gr.get_sommets()[i].getID()),(int)Xi+9,(int)Yi+17);

            // longueurs critiques
            if(_longueurCritiques != null)
            {
                g.setColor(Color.RED);
                g.drawString(Integer.toString(_longueurCritiques[i]),(int)Xi+9,(int)Yi+42);
                g.setColor(_defautColor);
            }

            // rangs
            if(_rangs != null)
            {
                g.setColor(Color.RED);
                g.drawString(Integer.toString(_rangs[i]),(int)Xi+9,(int)Yi+42);
                g.setColor(_defautColor);
            }

            // éditer les liens
            if(_couleurLiens != null)
            {
                setForeground(_couleurLiens); // couleur de coloration de liens choisie
            }
            if(_gr.nbLiens()>0)
            {
                int n = _gr.nbSommets();
                for (int j = 1; j<=n;j++)
                {
                    if(_gr.get_poidsAdjacence()[i][j] != Integer.MAX_VALUE) // lien existant entre i,j
                    {
                        if(!_gr.grapheOriente() && i<=j) // dessiner que dans un sens
                        {
                            g.setColor(Color.LIGHT_GRAY);
                            g.drawLine((int)_gr.get_sommets()[i].getX()+(_rayonSommet/2),(int)_gr.get_sommets()[i].getY()+(_rayonSommet/2),(int)_gr.get_sommets()[j].getX()+(_rayonSommet/2),(int)_gr.get_sommets()[j].getY()+(_rayonSommet/2));
                            g.setColor(Color.BLUE);
                        }
                        if(_gr.grapheOriente())
                        {
                            g.setColor(Color.LIGHT_GRAY);
                            g.drawLine((int)_gr.get_sommets()[i].getX()+(_rayonSommet/2),(int)_gr.get_sommets()[i].getY()+(_rayonSommet/2),(int)_gr.get_sommets()[j].getX()+(_rayonSommet/2),(int)_gr.get_sommets()[j].getY()+(_rayonSommet/2));
                            g.setColor(Color.BLUE);

                            double Xj = _gr.get_sommets()[j].getX();
                            double Yj = _gr.get_sommets()[j].getY();

                            double x;
                            double dx = (Xj) - Xi;
                            double dy = (Yj) - Yi;
                            double D = Math.sqrt(dx*dx + dy*dy);
                            double xm = D - FLECHE.width;
                            double ym = FLECHE.height;
                            double yn = -FLECHE.height;
                            double xn = xm;
                            double sin = dy / D, cos = dx / D;

                            x = xm * cos - ym * sin + Xi;
                            ym = xm * sin + ym * cos + Yi;
                            xm = x;
                            x = xn * cos - yn * sin + Xi;
                            yn = xn * sin + yn * cos + Yi;
                            xn = x;

                            int[] xpt = {(int)Xj+(_rayonSommet/2), (int) xm+(_rayonSommet/2), (int) xn+(_rayonSommet/2)};
                            int[] ypt = {(int)Yj+(_rayonSommet/2), (int) ym+(_rayonSommet/2), (int) yn+(_rayonSommet/2)};
                            g.setColor(Color.LIGHT_GRAY);
                            g.fillPolygon(xpt, ypt, 3);
                            g.setColor(_defautColor);
                        }

                        if(_gr.graphePondere())
                        {
                            // dessiner le poids
                            double posXPoids = 0;
                            double posYPoids = 0;
                            if(_gr.get_sommets()[i].getX() >= _gr.get_sommets()[j].getX())
                            {
                                posXPoids = ((_gr.get_sommets()[i].getX() - _gr.get_sommets()[j].getX())/2)+_gr.get_sommets()[j].getX() -5;
                            }
                            else
                            {
                                posXPoids = ((_gr.get_sommets()[j].getX() - _gr.get_sommets()[i].getX())/2)+_gr.get_sommets()[i].getX() +5;
                            }
                            if(_gr.get_sommets()[i].getY() >= _gr.get_sommets()[j].getY())
                            {
                                posYPoids = ((_gr.get_sommets()[i].getY() - _gr.get_sommets()[j].getY())/2)+_gr.get_sommets()[j].getY() -5;
                            }
                            else
                            {
                                posYPoids = ((_gr.get_sommets()[j].getY() - _gr.get_sommets()[i].getY())/2)+_gr.get_sommets()[i].getY() + 5;
                            }
                            if(_gr.grapheOriente())
                            {
                                g.drawString(Integer.toString(_gr.get_poidsAdjacence()[i][j]), (int)posXPoids, (int)posYPoids);
                            }
                            if(!_gr.grapheOriente() && i<=j) // dessiner que dans un sens
                            {
                                g.drawString(Integer.toString(_gr.get_poidsAdjacence()[i][j]), (int)posXPoids, (int)posYPoids);
                            }
                        }
                    }
                }
            }
        }
        _rafraichir = false;
    }

}
