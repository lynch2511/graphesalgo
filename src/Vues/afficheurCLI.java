package Vues;

import Graphe.Graphe;
import org.apache.commons.lang3.ArrayUtils;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class afficheurCLI{

    private Surface _d;
    private JFrame _f;
    private int _largeurPlan;
    private int _hauteurPlan;

    public afficheurCLI() {
    }

    public void afficherGraphe(Graphe gr)
    {
        _largeurPlan = 600;
        _hauteurPlan = 600;
        _d = new Surface(gr,_largeurPlan,_hauteurPlan);
        _f = new JFrame("Affichage du graphe");
        _f.add(_d);
        _f.setSize(_largeurPlan,_hauteurPlan);
        _f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        _f.setLocationRelativeTo(null);
        _f.setVisible(true);
    }

    public void afficherRangs(int[] rg)
    {
        if(_d != null && _largeurPlan != 0)
        {
            _d.afficherRangs(rg);
            _f.repaint();
        }
    }

    public void rafraichirColoration(Graphe gr, Color c)
    {
        if(_d != null && _largeurPlan != 0)
        {
            _d.rafraichir(gr,c);
            _f.repaint();
        }
    }

    public void rafraichirAffichage(Graphe gr)
    {
        if(_d != null && _largeurPlan != 0)
        {
            _d.rafraichir(gr,null);
            _f.repaint();
        }
    }

    public void afficherLC(int[] lc)
    {
        if(_d != null && _largeurPlan != 0)
        {
            _d.afficherLC(lc);
            _f.repaint();
        }
    }

    public static int afficherOptions(List<String> options, int indiceMin, int indiceMax)
    {
        Scanner sc = new Scanner(System.in);
        int choix=Integer.MIN_VALUE;
        while((choix < indiceMin || choix > indiceMax))
        {
            for(String o : options)
            {
                print(o);
            }
            print("\nVotre choix : \n");
            try
            {
                choix = sc.nextInt();
            }
            catch(InputMismatchException e)
            {
                print("Veuillez saisir un nombre!\n");
                sc.next();
            }
        }
        return choix;
    }

    public static String afficherSaisieFichier()
    {
        Scanner sc = new Scanner(System.in);
        String choix="";
        while(choix == "")
        {
            print("\n Veuillez renseigner le chemin absolu du fichier (SANS ESPACES ou back slash dans le chemin !) : \n");
            try
            {
                choix = sc.nextLine();
            }
            catch(InputMismatchException e)
            {
                print("Veuillez saisir une chaine de caracteres sur une ligne !\n");
                sc.next();
            }
        }
        return choix;
    }

    public int[] SaisieTab(String instructions)
    {
        Scanner sc = new Scanner(System.in);
        String choix= "";
        int[] tab = new int[1];
        while(choix=="")
        {
            print(instructions);
            try
            {
                choix = sc.nextLine();
                for(Character c : choix.toCharArray())
                {
                    if(c != ',' && c != ' ')
                    {
                        tab = ArrayUtils.add(tab,Character.getNumericValue(c));
                    }
                }
                tab[0] = tab.length-1;
            }
            catch(InputMismatchException e)
            {
                print("Veuillez saisir des entiers \n");
                sc.next();
            }
        }
        return tab;
    }

    public int[] afficherSaisieTab(int i)
    {
        int[] saisie = new int[0];
        switch(i)
        {
            case 16100: // saisie du tableau de codage prufer
                String instructions = "Veuillez saisir le tableau P du codage ce Prüfer avec le format suivant : valeur,valeur,(...) l\n";
                saisie = SaisieTab(instructions);
                break;
        }
        return saisie;
    }

    public int afficherMenu(int i, Graphe optionalgCourant, ArrayList data)
    {
        int numChoix = 0;
        switch (i)
        {

            case 0: // retour menu principal
                numChoix= afficherMenu(1,null,null);
                break;
            case 1: // Menu principal
                List<String> opt = new ArrayList<String>();
                opt.add("1- Créer un Graphe \n");
                opt.add("2- Importer un Graphe \n");
                opt.add("3- Afficher la liste des algorithmes et leur description\n");
                opt.add("4- Application\n");
                opt.add("5- Quitter\n");
                numChoix= afficherOptions(opt,1,5);
                break;
            case 11: // Créer un graphe oriente
                List<String> opt10 = new ArrayList<String>();
                opt10.add("Graphe orienté ?\n");
                opt10.add("1- Oui\n");
                opt10.add("2- Non\n");
                numChoix= afficherOptions(opt10,1,2);
                break;
            case 111: // Créer un graphe pondere
                List<String> opt11 = new ArrayList<String>();
                opt11.add("Graphe pondéré ?\n");
                opt11.add("1- Oui\n");
                opt11.add("2- Non\n");
                numChoix= afficherOptions(opt11,1,2);
                break;
            case 1111: // Graphe d'ordonnancement
                List<String> opt1111 = new ArrayList<String>();
                opt1111.add("Graphe d'ordonnancement ?\n");
                opt1111.add("1- Oui\n");
                opt1111.add("2- Non\n");
                numChoix= afficherOptions(opt1111,1,2);
                break;
            case 10: // Menu principal : gestion un graphe
                List<String> opt2 = new ArrayList<String>();
                opt2.add("0- Retour menu principal (sans sauvegarder) \n");
                opt2.add("11- Créer un sommet\n");
                if(optionalgCourant.nbSommets() != 0)
                {
                    opt2.add("12- Créer un lien\n");
                    opt2.add("13- Supprimer un sommet\n");
                    if(optionalgCourant.nbLiens() != 0)
                    {
                        opt2.add("14- Supprimer un lien\n");
                    }
                }
                opt2.add("15- Sauvegarder le graphe et quitter\n");
                opt2.add("16- Appliquer un algorithme\n");
                numChoix= afficherOptions(opt2,0,16);
                break;
            case 200: // Menu principal : Importer graphe
                List<String> opt4 = new ArrayList<String>();
                opt4.add("Graphe orienté ?\n");
                opt4.add("201- Oui\n");
                opt4.add("202- Non\n");
                numChoix= afficherOptions(opt4,201,202);
                break;
            case 2000: // Menu principal : Importer graphe
                List<String> opt5 = new ArrayList<String>();
                opt5.add("Structure de données dans le fichier: \n");
                opt5.add("2001- FS/APS\n");
                opt5.add("2002- Codage Prüfer\n");
                numChoix= afficherOptions(opt5,2001,2002);
                break;
            case 120: // Créer/supprimer un arc/arete
                List<String> opt7 = new ArrayList<String>();
                opt7.add("Saisir sommet de départ : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length; j++)
                {
                    opt7.add(optionalgCourant.get_sommets()[j].toString()+")\n");
                }
                numChoix= afficherOptions(opt7,1,optionalgCourant.get_sommets().length-1);
                break;
            case 1200: // Créer/supprimer un arc/arete
                List<String> opt8 = new ArrayList<String>();
                opt8.add("Saisir sommet d'arrivée : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length; j++)
                {
                    opt8.add(optionalgCourant.get_sommets()[j].toString()+")\n");
                }
                numChoix= afficherOptions(opt8,1,optionalgCourant.get_sommets().length-1);
                break;
            case 12000: // Créer un arc/arete pondéré
                List<String> opt6 = new ArrayList<String>();
                opt6.add("Veuillez saisir son poids de "+Integer.MIN_VALUE+1+" à "+(Integer.MAX_VALUE -1)+" : \n");
                numChoix= afficherOptions(opt6, Integer.MIN_VALUE+1,Integer.MAX_VALUE-1);
                break;
            case 12001: // Créer une durée
                List<String> opt12001 = new ArrayList<String>();
                opt12001.add("Veuillez saisir une durée pour la tâche "+data.get(0)+" de 1 à "+(Integer.MAX_VALUE -1)+" : \n");
                numChoix= afficherOptions(opt12001, 1,Integer.MAX_VALUE-1);
                break;
            case 14: // Supprimer un sommet
                List<String> opt9 = new ArrayList<String>();
                opt9.add("Saisir le sommet à supprimer : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length; j++)
                {
                    opt9.add(optionalgCourant.get_sommets()[j].toString()+")\n");
                }
                numChoix= afficherOptions(opt9,1,optionalgCourant.get_sommets().length-1);
                break;
            case 150: // Sauvegarder le graphe
                List<String> opt16 = new ArrayList<String>();
                if(optionalgCourant.algoApplicable(3))
                {
                    opt16.add("1-- Sauvegarder avec le codage de Prufer\n");
                }
                opt16.add("2-- Sauvegarder avec la structure FS/APS\n");
                numChoix = afficherOptions(opt16,1,2);
                break;
            case 160:
                List<String> opt12 = new ArrayList<String>();
                opt12.add("Saisir un algorithme : \n");
                for(int j = 1; j< data.size();j++)
                {
                    if(optionalgCourant.algoApplicable(j)) // on affiche si l'algo est applicable au graphe
                    {
                        opt12.add(j+"-- "+data.get(j).toString()+")\n");
                    }
                }
                opt12.add("0- Retour au menu principal");
                numChoix= afficherOptions(opt12,0,data.size()-1);
                break;
            case 1610:
                List<String> opt15 = new ArrayList<String>();
                opt15.add("Effectuez un choix : \n");
                opt15.add("1-- Décodage de prufer: \n");
                opt15.add("2-- Coder le graphe courant : \n");
                opt15.add("0- Retour au menu principal");
                numChoix= afficherOptions(opt15,0,2);
                break;
            case 16020:
                List<String> opt13 = new ArrayList<String>();
                opt13.add("Choisir le sommet de départ : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length;j++)
                {
                        opt13.add(j+")\n");
                }
                numChoix= afficherOptions(opt13,1,optionalgCourant.get_sommets().length-1);
                break;
            case 160200:
                List<String> opt14 = new ArrayList<String>();
                opt14.add("Choisir le sommet de d'arrivée : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length;j++)
                {
                    opt14.add(j+")\n");
                }
                numChoix= afficherOptions(opt14,1,optionalgCourant.get_sommets().length-1);
                break;
            case 1630:
                List<String> opt17 = new ArrayList<String>();
                opt17.add("1-- Retour au graphe initial");
                numChoix= afficherOptions(opt17,1,1);
                break;
            case 40: // menu application
                List<String> opt40 = new ArrayList<String>();
                opt40.add("Choisir la ville de départ : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length;j++)
                {
                    opt40.add(j+") "+optionalgCourant.get_sommets()[j].getData()[0]+"\n");
                }
                numChoix= afficherOptions(opt40,1,optionalgCourant.get_sommets().length-1);
                break;
            case 400:
                List<String> opt400 = new ArrayList<String>();
                opt400.add("Choisir la ville d'arrivée : \n");
                for(int j = 1; j< optionalgCourant.get_sommets().length;j++)
                {
                    opt400.add(j+") "+optionalgCourant.get_sommets()[j].getData()[0]+"\n");
                }
                numChoix= afficherOptions(opt400,1,optionalgCourant.get_sommets().length-1);
                break;
        }
        return numChoix;
    }

    public void afficherMat(int[][] g)
    {
        if(g != null)
        {
            for(int i =0; i < g.length; i++)
            {
                for (int j = 0; j < g[0].length; j++)
                {
                    print(Integer.toString(g[i][j])+" ");
                }
                print("\n");
            }
        }
    }

    public void afficherTab(int[] t)
    {
        for (Object o : t) {
            print(o.toString()+" ");
        }print("\n");

    }



    public static void print(String s)
    {
        System.out.print(s);
    }

}
