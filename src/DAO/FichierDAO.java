package DAO;

import Graphe.Graphe;
import Modeles.AlgoPrufer;
import Modeles.Algorithme;
import Vues.afficheurCLI;
import com.hs.gpxparser.GPXParser;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.Waypoint;
import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FichierDAO {

    public static enum formats{
        PRUFER, FSAPS
    }

    public static boolean fichierExistant(File f)
    {
        if(!f.exists()) { afficheurCLI.print("Fichier non existant!"); return false;}
        return true;
    }

    public static int[][] lectureFichier(String chem)
    {
        File file = new File(chem);
        int[][] out;
        if(!fichierExistant(file)) { return null;}
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out = new int[0][0];
        int[] code = new int[0];
        try
        {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                while (lineScanner.hasNext()) {
                    int i = lineScanner.nextInt();
                    code = ArrayUtils.add(code,i);
                }
                lineScanner.close();
                if(code.length > 0)
                {
                    out = ArrayUtils.add(out,code);
                }
                code = new int[0];
            }
        }
        catch(InputMismatchException e)
        {
            afficheurCLI.print(e.getMessage());
        }
        return out;
    }

    /**
     * Ecrit dans un fichier le contenu de la matrice en paramètre : Chaque ligne i de la matrice = une ligne dans le fichier
     * @param outPath
     * @return opération réussie ?
     */
    public static boolean ecrireGrapheFichier(String outPath, Graphe g, formats f, ArrayList<Algorithme> listeAlgos)
    {
        int[][] out = new int[1][1];
        out[0] = new int[4];
        out[0][1] = 0;
        out[0][2] = 0;
        out[0][3] = 0;
        if(g.grapheOriente()) {out[0][1] = 1;}
        if(g.graphePondere()) {out[0][2] = 1;}
        if(g.grapheOrdonnancement()){out[0][3] = 1;}
        switch(f)
        {
            case FSAPS:
                g.poidsADJ_TO_FSAPS();
                out[0][0] = formats.FSAPS.ordinal()+1; // la première ligne contient l'indice du type de format
                if(g.get_fs() != null && g.get_aps() != null)
                {
                    out = ArrayUtils.add(out,g.get_fs());
                    out = ArrayUtils.add(out,g.get_aps());
                }
                if(g.graphePondere())
                {
                    int[][] matpoids = g.getMatricePoids();
                    if(matpoids != null)
                    {
                        for(int[] m : matpoids)
                            out = ArrayUtils.add(out,m);
                    }
                }
                break;
            case PRUFER:
                AlgoPrufer ap = new AlgoPrufer(listeAlgos.get(3).getNom(),listeAlgos.get(3).getDescription(),g.get_poidsAdjacence());
                int[] resPrufer = ap.CodageAlgoPrufer();
                out[0][0] = formats.PRUFER.ordinal()+1;
                if(resPrufer != null){
                    out = ArrayUtils.add(out,resPrufer);
                }
                break;
        }
        return ecritureFichier(outPath,out);
    }

    public static boolean sauvegarderResultat(int[][] res, Algorithme a)
    {
        String outPath =System.getProperty("user.dir");
        File file = new File(outPath);
        if (!file.isDirectory())
        {
            afficheurCLI.print("Répertoire courant non valide!");
            return false;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-hh-mm-ss");
        String filePath = outPath+"/resultat"+a.getNom()+"-"+ ZonedDateTime.now().format(formatter)+".txt";
        try
        {
            // Création
            File target = new File(filePath);
            target.delete();
            Path fp = Paths.get(filePath);
            Files.createFile(fp);
            // écriture
            PrintWriter writer;
            writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath)));
            for (int i = 0; i < res.length; i++)
            {
                // sur une ligne, on écrit un tableau
                String line = "";
                for (int j = 0; j < res[i].length; j++)
                {
                    line += Integer.toString(res[i][j])+" ";
                }
                writer.println(line);
            }
            writer.close();
            return true;
        }
        catch (Exception e)
        {
            System.err.println(e);
            return false;
        }
    }

    public static boolean ecritureFichier(String outPath, int[][] out)
    {
        File file = new File(outPath);
        if (!file.isDirectory())
        {
            afficheurCLI.print("Répertoire non valide!");
            return false;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-hh-mm-ss");
        String filePath = outPath+"/graphe"+ ZonedDateTime.now().format(formatter)+".txt";
        try
        {
            // Création
            File target = new File(filePath);
            target.delete();
            Path fp = Paths.get(filePath);
            Files.createFile(fp);
            // écriture
            PrintWriter writer;
            writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath)));
            for (int i = 0; i < out.length; i++)
            {
                // sur une ligne, on écrit un tableau
                String line = "";
                for (int j = 0; j < out[i].length; j++)
                {
                    line += Integer.toString(out[i][j])+" ";
                }
                writer.println(line);
            }
            writer.close();
            return true;
        }
        catch (Exception e)
        {
            System.err.println(e);
            return false;
        }
    }

    public void gpxParser()
    {
        GPXParser p = new GPXParser();
        FileInputStream in = null;
        try {
            in = new FileInputStream("besancon-mulhouse-1.gpx");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        HashSet<Waypoint> wp;
        HashSet<Track> traks;
        HashMap<String, String> d;
        try {
            GPX gpx = p.parseGPX(in);
            traks =gpx.getTracks();
            d = gpx.getXmlns();

            //Iterator i = traks.iterator();
            //while(i)
            //{
            //  Object pp =  i.next();
            //window.addPOI(new POI(new Point(pp.getLatitude(),pp.getLongitude()),""));
            //}

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
